//e2ePageObject.po.js
var URL = 'https://www.google.com/';

var GooglePage = function(){

this.googleSearchField = element(by.css('.gLFyf'));
this.googleFirstResultLink = element(by.xpath('//h3'));

};


GooglePage.prototype.visit = function () {
  browser.get(URL);
  };

module.exports = GooglePage;