// This code is functional too, it needs Protractor and Selenium to be installed globally on your machine.

var GooglePage = require('../pageObjects/e2ePageObject.po.js');

 describe('A simple e2e test', function (){
   var googlePage = new GooglePage();

   it('Searching for a term', async function (){
     googlePage.visit();
     googlePage.googleSearchField.sendKeys('AAAS');
     browser.actions().sendKeys(protractor.Key.ENTER).perform();
     googlePage.googleFirstResultLink.click();
     expect(browser.getCurrentUrl()).toContain('https://www.aaas.org/');
    });
 });
