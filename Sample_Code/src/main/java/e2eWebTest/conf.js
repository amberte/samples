module.exports.config = {
    directConnect: true,
    capabilities: {
        'browserName': 'chrome'
        },
        specs: ['testSpecs/*.spec.js'],
        baseURL: 'https://www.google.com',
        onPrepare: function () {
            browser.driver.manage().window().maximize();
            browser.waitForAngularEnabled(false);
            },
};
