// This code is completely functional, you can test it if you want. :)

package APITestSample;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.lessThan;

import org.testng.annotations.*;

public class RestAssuredSample {
    @Test
    public void getPlans(){
        baseURI = "https://restcountries.eu/rest/v2/name/";

        given()
                .contentType("application/json")
                .when()
                    .get("Norway?fullText=true")
                .then()
                    .time(lessThan(2000L))
                .assertThat()
                    .statusCode(200);
    }
}
